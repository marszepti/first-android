// Main.dart

import 'package:flutter/material.dart';

import './email.dart' as email;
import './music.dart' as music;
import './shopping.dart' as shopping;
import './telepon.dart' as telepon;

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "Tampilan Tab Bar",
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  //create controller untuk tabBar
  TabController controller;

  @override
  void initState() {
    controller = new TabController(vsync: this, length: 4);
    //tambahkan SingleTickerProviderStateMikin pada class _HomeState
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      //create appBar
      appBar: new AppBar(
        //warna background
        backgroundColor: Colors.lightGreen,
        //judul
        title: new Text("Tampilan Home "),
        //bottom
        bottom: new TabBar(
          controller: controller,
          //source code lanjutan
          tabs: <Widget>[
            new Tab(
              icon: new Icon(Icons.email),
              text: "Email",
            ),
            new Tab(
              icon: new Icon(Icons.queue_music),
              text: "Music",
            ),
            new Tab(
              icon: new Icon(Icons.shopping_cart),
              text: "Shop",
            ),
            new Tab(
              icon: new Icon(Icons.phone_android),
              text: "Phone",
            ),
          ],
        ),
      ),
      body: new TabBarView(
        controller: controller,
        children: <Widget>[
          new email.Email(),
          new music.Music(),
          new shopping.Shopping(),
          new telepon.Telepon()
        ],
      ),
      bottomNavigationBar: new Material(
        color: Colors.lightGreen,
        child: new TabBar(
          controller: controller,
          tabs: <Widget>[
            //copy saja pada bagian atas tab bar
            //hilangkan text agar lebih simple atau sesuka Anda
            new Tab(
              icon: new Icon(Icons.email),
            ),
            new Tab(
              icon: new Icon(Icons.queue_music),
            ),
            new Tab(
              icon: new Icon(Icons.shopping_cart),
            ),
            new Tab(
              icon: new Icon(Icons.phone_android),
            ),
          ],
        ),
      ),
    );
  }
}


// Telepon.Dart

import 'package:flutter/material.dart';

class Telepon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      body: ListView(children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              'assets/1.jpg',
              height: 100,
              width: 100,
            ),
            Image.asset(
              'assets/2.jpg',
              height: 100,
              width: 100,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              'assets/3.jpg',
              height: 100,
              width: 100,
            ),
            Image.asset(
              'assets/4.jpg',
              height: 100,
              width: 100,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              'assets/5.jpg',
              height: 100,
              width: 100,
            ),
            Image.asset(
              'assets/6.jpg',
              height: 100,
              width: 100,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              'assets/7.jpg',
              height: 100,
              width: 100,
            ),
            Image.asset(
              'assets/8.jpg',
              height: 100,
              width: 100,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              'assets/9.jpg',
              height: 100,
              width: 100,
            ),
            Image.asset(
              'assets/10.jpg',
              height: 100,
              width: 100,
            ),
          ],
        ),
      ]),
    );
  }
}

// Shopping.dart

import 'package:flutter/material.dart';

class Shopping extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'First App',
      home: Scaffold(
          backgroundColor: Colors.purpleAccent,
          body: ListView(children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/nacific.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Nacific',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.250.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/nacific2.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Nacific',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.260.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/makeover1.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Makeover',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.300.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/makeover2.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Makeover',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.160.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/wardah1.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Wardah',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.120.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/wardah2.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Wardah',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.200.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/safi1.JPG',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Safi',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.250.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/safi2.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Safi',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.260.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/emina1.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Emina',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.60.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/emina2.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Emina',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Rp.80.000',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
          ])),
    );
  }
}


// Email.dart

import 'package:flutter/material.dart';

class Email extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'First App',
      home: Scaffold(
          backgroundColor: Colors.yellowAccent,
          body: Column(children: <Widget>[
            Text(
              'Covid 19',
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Positif',
                        style: TextStyle(
                            color: Colors.brown,
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Jumlah Positif : 100',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.lightBlueAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Sembuh',
                        style: TextStyle(
                            color: Colors.brown,
                            fontSize: 19.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Jumlah Sembuh : 200',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.lightGreen),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Meninggal',
                        style: TextStyle(
                            color: Colors.brown,
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Jumlah Meninggal : 50',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.pink),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Global',
                        style: TextStyle(
                            color: Colors.brown,
                            fontSize: 19.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Jumlah Positif : 100',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Jumlah Sembuh : 200',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Jumlah Meninggal : 50',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.purpleAccent),
                ),
              ],
            ),
          ])),
    );
  }
}


// Music.dart

import 'package:flutter/material.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Music(),
    );
  }
}

class Music extends StatelessWidget {
  final List music = [
    "BTS - Fire",
    "EXO - Kokobop",
    "Rihanna - Diamond",
    "Cardi B - I like it",
    "Bruno Mars - Lazy",
    "Zayn - Pillow Talk",
    "Charlie Puth - Attention",
    "Rizky Febian - Cuek",
    "St12 - Saat Terakhir",
    "Shawn Mendes - Imagination",
    "Blackpink - So hot",
    "Red Velvet - Bad Boy"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (context, index) {
          return Card(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text(music[index], style: TextStyle(fontSize: 20)),
            ),
          );
        },
        itemCount: music.length,
      ),
    );
  }
}

// pubspec
- assets/covid.png
  - assets/me.jpg
  - assets/boru.png
  - assets/hima.jpg
  - assets/family.jpg
  - assets/hinata.jpg
  - assets/naruto.jpg
  - assets/nacific.jpg
  - assets/nacific2.jpg
  - assets/makeover1.jpg
  - assets/makeover2.jpg
  - assets/wardah1.jpg
  - assets/wardah2.jpg
  - assets/safi1.JPG
  - assets/safi2.jpg
  - assets/emina1.jpg
  - assets/emina2.jpg
  - assets/1.jpg
  - assets/2.jpg
  - assets/3.jpg
  - assets/4.jpg
  - assets/5.jpg
  - assets/6.jpg
  - assets/7.jpg
  - assets/8.jpg
  - assets/9.jpg
  - assets/10.jpg
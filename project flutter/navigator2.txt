import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      title: 'Navigation Basic',
      home: HalamanPertama(),
    ));

class HalamanPertama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                child: RaisedButton(
                  child: Text('Halaman Pertama'),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HalamanUtama()));
                  },
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                child: RaisedButton(
                  child: Text('Halaman Kedua'),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HalamanKedua()));
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

//CLASS HALAMAN KEDUA

class HalamanUtama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Scaffold(
          backgroundColor: Colors.yellowAccent,
          appBar: AppBar(
            title: Text('Naruto Family'),
            backgroundColor: Colors.greenAccent,
          ),
          body: ListView(children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset(
                  'assets/naruto.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/family.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/hinata.jpg',
                  height: 100,
                  width: 100,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset(
                  'assets/naruto.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/family.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/hinata.jpg',
                  height: 100,
                  width: 100,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset(
                  'assets/naruto.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/family.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/hinata.jpg',
                  height: 100,
                  width: 100,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset(
                  'assets/naruto.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/family.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/hinata.jpg',
                  height: 100,
                  width: 100,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset(
                  'assets/naruto.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/family.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/hinata.jpg',
                  height: 100,
                  width: 100,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset(
                  'assets/naruto.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/family.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/hinata.jpg',
                  height: 100,
                  width: 100,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset(
                  'assets/naruto.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/family.jpg',
                  height: 100,
                  width: 100,
                ),
                Image.asset(
                  'assets/hinata.jpg',
                  height: 100,
                  width: 100,
                ),
              ],
            ),
          ])),
    );
  }
}

class HalamanKedua extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // debugShowCheckedModeBanner: false,
      title: 'First App',
      home: Scaffold(
          //backgroundColor: Colors.purpleAccent,
          appBar: AppBar(
            title: Text('Card View Boruto'),
            backgroundColor: Colors.redAccent,
          ),
          body: ListView(children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/boru.png',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Boruto',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Uzumaki',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/hima.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Himawari',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Uzumaki',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/boru.png',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Boruto',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Uzumaki',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/hima.jpg',
                        height: 100,
                        width: 100,
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Himawari',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Uzumaki',
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  height: 180,
                  width: 160,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.all(9.0),
                  decoration: BoxDecoration(color: Colors.yellowAccent),
                ),
              ],
            ),
          ])),
    );
  }
}
